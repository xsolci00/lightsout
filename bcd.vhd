----------------------------------------------------------------------------------
-- Company: 
-- Engineer: 
-- 
-- Create Date:    15:31:05 03/31/2016 
-- Design Name: 
-- Module Name:    bcd - Behavioral 
-- Project Name: 
-- Target Devices: 
-- Tool versions: 
-- Description: 
--
-- Dependencies: 
--
-- Revision: 
-- Revision 0.01 - File Created
-- Additional Comments: 
--
----------------------------------------------------------------------------------
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.STD_LOGIC_unsigned.ALL;

-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
--use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx primitives in this code.
--library UNISIM;
--use UNISIM.VComponents.all;

entity bcd is
    Port ( CLK : in  STD_LOGIC;
           RESET : in  STD_LOGIC;
           NUMBER3 : buffer  std_logic_vector(3 downto 0) := "0000";
           NUMBER2 : buffer  std_logic_vector(3 downto 0) := "0000";
           NUMBER1 : buffer  std_logic_vector(3 downto 0) := "0000"
			  );
			  
end bcd;

architecture Behavioral of bcd is
-- signal NUMBER3_tmp: std_logic_vector(0 to 3);
-- signal NUMBER2_tmp: std_logic_vector(0 to 3);
-- signal NUMBER1_tmp: std_logic_vector(0 to 3);
begin
process(CLK,RESET)
begin
	if Reset='1' then
			NUMBER3 <= "0000";
			NUMBER2 <= "0000";
			NUMBER1 <= "0000";
	elsif(rising_edge(CLK)) then


		if NUMBER1="1001" and NUMBER2="1001" then
				NUMBER3 <= "0001";
				NUMBER2 <= "0000";
				NUMBER1 <= "0000";
		elsif NUMBER1="1001" then
				NUMBER2 <= NUMBER2 + 1;
				NUMBER1 <= "0000";
		else
			--podminka zda neni NUMBER3 == 1 aby sme predesli inkrementaci hned po nulovani dosazeni 100
				NUMBER1 <= NUMBER1 + 1;

		if NUMBER3="0001" then
				NUMBER3 <= "0000";
				NUMBER2 <= "0000";
				NUMBER1 <= "0000";

		end if;
		end if;
		end if;


   end process;
--   NUMBER3 <= NUMBER3_tmp;
--	NUMBER2 <= NUMBER2_tmp;
--	NUMBER1 <= NUMBER1_tmp;
end Behavioral;

