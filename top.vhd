library ieee;

use ieee.std_logic_1164.all;
use ieee.std_logic_arith.all;
use ieee.std_logic_unsigned.all;
use work.vga_controller_cfg.all;
use work.mask_pack.ALL;

--###############################################################################################################

architecture main of tlv_pc_ifc is
	
      			
 			  	
 			 
	--=========================================================================
	constant IDX_TOP    : NATURAL := 0;
	constant IDX_LEFT   : NATURAL := 1;
	constant IDX_RIGHT  : NATURAL := 2;
	constant IDX_BOTTOM : NATURAL := 3;
	constant IDX_ENTER  : NATURAL := 4; 

	--=========================================================================
	signal vga_mode   : std_logic_vector(60 downto 0); -- default 640x480x60

	signal irgb 	  : std_logic_vector(8 downto 0);
	signal rgb_activ 	  : std_logic_vector(8 downto 0) := "111011000"; --orange (active)
	signal  rgb_notactiv 	  : std_logic_vector(8 downto 0) := "000000111"; --blue (not active)
	--signal  sel 	  : std_logic_vector(8 downto 0) := "111000000";
	signal  rgb_sel_act : std_logic_vector(8 downto 0) := "111000000"; --red (selected + active)
	signal  rgb_sel  : std_logic_vector(8 downto 0) := "011100011"; --grey (selected + not active)

	alias red   is irgb(8 downto 6);
	alias green is irgb(5 downto 3);
	alias blue  is irgb(2 downto 0);

	signal row 	  : std_logic_vector(11 downto 0);
	signal col    : std_logic_vector(11 downto 0);
	signal offset : std_logic_vector(11 downto 0) := "000000011110";

	--=========================================================================
	
	signal init_active   : std_logic_vector(24 downto 0) := "0000000100011100010000000";
	signal game1   : std_logic_vector(24 downto 0) := "0000000100011100010000000";
	signal game2   : std_logic_vector(24 downto 0) := "1100000100000000010000011";
	signal game3   : std_logic_vector(24 downto 0) := "0001100100011100100110000";
	signal game4   : std_logic_vector(24 downto 0) := "0000000000111111000000000";

	signal init_selected : std_logic_vector(24 downto 0) := "0000000000000000000000001";

	--=========================================================================
	-- 25 bit vga display
	signal vga_active   : std_logic_vector(24 downto 0);
	signal vga_select : std_logic_vector(24 downto 0);

	--=========================================================================
	-- KEYBOARD SIGNALS
	signal kbrd_data_out : std_logic_vector(15 downto 0);
	signal keys: std_logic_vector(4 downto 0);


	signal select_request : std_logic_vector(99 downto 0) := "0000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000"; 
	signal ivert_request : std_logic_vector(99 downto 0) := "0000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000";

	signal RESET_CELL : STD_LOGIC := '1';

	signal en_1MHz,en_keyHz : std_logic;
begin

gen_1mhz: entity work.engen generic map ( MAXVALUE => 39) port map ( CLK => CLK, ENABLE => '1', EN => en_1MHz );
gen_keyHz: entity work.engen generic map ( MAXVALUE => 70000) port map ( CLK => CLK, ENABLE => en_1MHz, EN => en_keyHz );
--gen_20hz: entity work.engen generic map ( MAXVALUE => 12500) port map ( CLK => CLK, ENABLE => en_1MHz, EN => en_20Hz );
	--=========================================================================
	-- PORTING OF KEYBOARD
	kbrd_ctrl: entity work.keyboard_controller(arch_keyboard)
	port map (
		CLK => CLK,
		RST => RESET,

		DATA_OUT => kbrd_data_out(15 downto 0),
		
		KB_KIN   => KIN,
		KB_KOUT  => KOUT
	);

	--=========================================================================



	riadky: for y in 0 to 4 generate
	begin  
		stlpce:  for x in 0 to 4 generate 
			begin
   			cell: entity work.cell 
   			generic map (
   				MASK => getmask(x,y,5,5)
   				)
   			port map (
	
				SELECTED => vga_select(y*5+x),
				ACTIVE 	 => vga_active(y*5+x),

				INIT_ACTIVE   => init_active(y*5+x),
				INIT_SELECTED => init_selected(y*5+x),

				SELECT_REQ_OUT => select_request((((y*5+x)*4 ) +3) downto ( (y*5+x)*4+0) ),
				INVERT_REQ_OUT => ivert_request((y*5+x)*4+3 downto (y*5+x)*4+0),

				SELECT_REQ_IN(IDX_TOP)    => select_request((((y-1) mod 5)*5 + x)*4 + IDX_BOTTOM),
				SELECT_REQ_IN(IDX_RIGHT)  => select_request((y*5 +((x+1) mod 5))*4 + IDX_LEFT),
				SELECT_REQ_IN(IDX_LEFT)   => select_request((y*5 +((x-1) mod 5))*4 + IDX_RIGHT),
				SELECT_REQ_IN(IDX_BOTTOM) => select_request((((y+1) mod 5)*5 + x)*4 + IDX_TOP),

				INVERT_REQ_IN(IDX_TOP)    => ivert_request((((y-1) mod 5)*5 + x)*4 + IDX_BOTTOM),
				INVERT_REQ_IN(IDX_RIGHT)  => ivert_request((y*5 + ((x+1) mod 5))*4 + IDX_LEFT),
				INVERT_REQ_IN(IDX_LEFT)   => ivert_request((y*5 + ((x-1) mod 5))*4 + IDX_RIGHT),
				INVERT_REQ_IN(IDX_BOTTOM) => ivert_request((((y+1) mod 5)*5 + x)*4 + IDX_TOP),

				CLK   => CLK,
				RESET => RESET_CELL,
				KEYS  => keys(4 downto 0)
			);

			--MASK=get_mask(x,y,5,5);

		end generate stlpce;
	end generate riadky;

	--=========================================================================
	-- Nastaveni grafickeho rezimu (640x480, 60 Hz refresh)
	setmode(r640x480x60, vga_mode);

	vga: entity work.vga_controller(arch_vga_controller) 
		generic map (REQ_DELAY => 1)
		port map ( 
			CLK    		=> CLK, 
			RST    		=> RESET,
			ENABLE 		=> '1',
			MODE   		=> vga_mode,

			DATA_RED    => red,
			DATA_GREEN  => green,
			DATA_BLUE   => blue,
			ADDR_COLUMN => col,
			ADDR_ROW    => row,

			VGA_RED     => RED_V,
			VGA_BLUE    => BLUE_V,
			VGA_GREEN 	=> GREEN_V,
			VGA_HSYNC 	=> HSYNC_V,
			VGA_VSYNC 	=> VSYNC_V
		);

	--=========================================================================
	-- KEY PRESS PROCESS
	klavesy: process(CLK)
	begin
		if rising_edge(CLK) then
		  keys <= "00000";
		  if (en_keyHz='1') then
			
			if kbrd_data_out(1)='1' then     -- key 6
				RESET_CELL <= '0';
				keys(IDX_LEFT)<='1';
			elsif kbrd_data_out(9)='1' then  -- key 4
				RESET_CELL <= '0';
				keys(IDX_RIGHT)<='1';
			elsif kbrd_data_out(4)='1' then  -- key 2
				RESET_CELL <= '0';
				keys(IDX_TOP)<='1';
			elsif kbrd_data_out(6)='1' then  -- key 8
				RESET_CELL <= '0';
				keys(IDX_BOTTOM)<='1';
			elsif kbrd_data_out(5)='1' then  -- key 5
				RESET_CELL <= '0';
				keys(IDX_ENTER) <= '1';
			elsif kbrd_data_out(12)='1' then -- key A, reset
				init_active <= game1;
				RESET_CELL <= '1';
			elsif kbrd_data_out(13)='1' then -- key B, reset
				init_active <= game2;
				RESET_CELL <= '1';
			elsif kbrd_data_out(14)='1' then -- key C, reset
				init_active <= game3;
				RESET_CELL <= '1';
			elsif kbrd_data_out(15)='1' then -- key D, reset
				init_active <= game4;
				RESET_CELL <= '1';
			end if;

		end if;
	  end if;
	end process;

	--=========================================================================
	-- GRAPHIC PROCESS
	process (CLK)
	begin  
		--RESET_CELL <= '0';
		if rising_edge(CLK) then

			-- generovani ctvercu a zmena jejich barev podle podminek

			if (row(11 downto 6) = "000000001") and (col(11 downto 6) = "000000010") then -- 1
				if (vga_active(0) = '1' and vga_select(0) = '1') then
					irgb <= rgb_sel_act; 
				elsif (vga_active(0) = '0' and vga_select(0) = '1') then
					irgb <= rgb_sel; 
				elsif (vga_active(0) = '1' and vga_select(0) = '0') then
					irgb <= rgb_activ; 
				else
					irgb <= rgb_notactiv;
				end if;

			elsif (row(11 downto 6) = "000000001") and (col(11 downto 6) = "000000011") then
				if (vga_active(1) = '1' and vga_select(1) = '1') then
					irgb <= rgb_sel_act; 
				elsif (vga_active(1) = '0' and vga_select(1) = '1') then
					irgb <= rgb_sel; 
				elsif (vga_active(1) = '1' and vga_select(1) = '0') then
					irgb <= rgb_activ; 
				else
					irgb <= rgb_notactiv;
				end if;

			elsif (row(11 downto 6) = "000000001") and (col(11 downto 6) = "000000100") then
				if (vga_active(2) = '1' and vga_select(2) = '1') then
					irgb <= rgb_sel_act; 
				elsif (vga_active(2) = '0' and vga_select(2) = '1') then
					irgb <= rgb_sel; 
				elsif (vga_active(2) = '1' and vga_select(2) = '0') then
					irgb <= rgb_activ; 
				else
					irgb <= rgb_notactiv;
				end if;

			elsif (row(11 downto 6) = "000000001") and (col(11 downto 6) = "000000101") then
				if (vga_active(3) = '1' and vga_select(3) = '1') then
					irgb <= rgb_sel_act; 
				elsif (vga_active(3) = '0' and vga_select(3) = '1') then
					irgb <= rgb_sel; 
				elsif (vga_active(3) = '1' and vga_select(3) = '0') then
					irgb <= rgb_activ; 
				else
					irgb <= rgb_notactiv;
				end if;
			elsif (row(11 downto 6) = "000000001") and (col(11 downto 6) = "000000110") then -- 5
				if (vga_active(4) = '1' and vga_select(4) = '1') then
					irgb <= rgb_sel_act; 
				elsif (vga_active(4) = '0' and vga_select(4) = '1') then
					irgb <= rgb_sel; 
				elsif (vga_active(4) = '1' and vga_select(4) = '0') then
					irgb <= rgb_activ; 
				else
					irgb <= rgb_notactiv;
				end if;

			elsif (row(11 downto 6) = "000000010") and (col(11 downto 6) = "000000010") then
				if (vga_active(5) = '1' and vga_select(5) = '1') then
					irgb <= rgb_sel_act; 
				elsif (vga_active(5) = '0' and vga_select(5) = '1') then
					irgb <= rgb_sel; 
				elsif (vga_active(5) = '1' and vga_select(5) = '0') then
					irgb <= rgb_activ; 
				else
					irgb <= rgb_notactiv;
				end if;
			
			elsif (row(11 downto 6) = "000000010") and (col(11 downto 6) = "000000011") then
				if (vga_active(6) = '1' and vga_select(6) = '1') then
					irgb <= rgb_sel_act; 
				elsif (vga_active(6) = '0' and vga_select(6) = '1') then
					irgb <= rgb_sel; 
				elsif (vga_active(6) = '1' and vga_select(6) = '0') then
					irgb <= rgb_activ; 
				else
					irgb <= rgb_notactiv;
				end if;
			
			elsif (row(11 downto 6) = "000000010") and (col(11 downto 6) = "000000100") then
				if (vga_active(7) = '1' and vga_select(7) = '1') then
					irgb <= rgb_sel_act; 
				elsif (vga_active(7) = '0' and vga_select(7) = '1') then
					irgb <= rgb_sel; 
				elsif (vga_active(7) = '1' and vga_select(7) = '0') then
					irgb <= rgb_activ; 
				else
					irgb <= rgb_notactiv;
				end if;
			
			elsif (row(11 downto 6) = "000000010") and (col(11 downto 6) = "000000101") then
				if (vga_active(8) = '1' and vga_select(8) = '1') then
					irgb <= rgb_sel_act; 
				elsif (vga_active(8) = '0' and vga_select(8) = '1') then
					irgb <= rgb_sel; 
				elsif (vga_active(8) = '1' and vga_select(8) = '0') then
					irgb <= rgb_activ; 
				else
					irgb <= rgb_notactiv;
				end if;
			
			elsif (row(11 downto 6) = "000000010") and (col(11 downto 6) = "000000110") then -- 10
				if (vga_active(9) = '1' and vga_select(9) = '1') then
					irgb <= rgb_sel_act; 
				elsif (vga_active(9) = '0' and vga_select(9) = '1') then
					irgb <= rgb_sel; 
				elsif (vga_active(9) = '1' and vga_select(9) = '0') then
					irgb <= rgb_activ; 
				else
					irgb <= rgb_notactiv;
				end if;

			elsif (row(11 downto 6) = "000000011") and (col(11 downto 6) = "000000010") then
				if (vga_active(10) = '1' and vga_select(10) = '1') then
					irgb <= rgb_sel_act; 
				elsif (vga_active(10) = '0' and vga_select(10) = '1') then
					irgb <= rgb_sel; 
				elsif (vga_active(10) = '1' and vga_select(10) = '0') then
					irgb <= rgb_activ; 
				else
					irgb <= rgb_notactiv;
				end if;
			
			elsif (row(11 downto 6) = "000000011") and (col(11 downto 6) = "000000011") then
				if (vga_active(11) = '1' and vga_select(11) = '1') then
					irgb <= rgb_sel_act; 
				elsif (vga_active(11) = '0' and vga_select(11) = '1') then
					irgb <= rgb_sel; 
				elsif (vga_active(11) = '1' and vga_select(11) = '0') then
					irgb <= rgb_activ; 
				else
					irgb <= rgb_notactiv;
				end if;
			
			elsif (row(11 downto 6) = "000000011") and (col(11 downto 6) = "000000100") then
				if (vga_active(12) = '1' and vga_select(12) = '1') then
					irgb <= rgb_sel_act; 
				elsif (vga_active(12) = '0' and vga_select(12) = '1') then
					irgb <= rgb_sel; 
				elsif (vga_active(12) = '1' and vga_select(12) = '0') then
					irgb <= rgb_activ; 
				else
					irgb <= rgb_notactiv;
				end if;
			
			elsif (row(11 downto 6) = "000000011") and (col(11 downto 6) = "000000101") then
			  if (vga_active(13) = '1' and vga_select(13) = '1') then
					irgb <= rgb_sel_act; 
				elsif (vga_active(13) = '0' and vga_select(13) = '1') then
					irgb <= rgb_sel; 
				elsif (vga_active(13) = '1' and vga_select(13) = '0') then
					irgb <= rgb_activ; 
				else
					irgb <= rgb_notactiv;
				end if;
			
			elsif (row(11 downto 6) = "000000011") and (col(11 downto 6) = "000000110") then -- 15
				if (vga_active(14) = '1' and vga_select(14) = '1') then
					irgb <= rgb_sel_act; 
				elsif (vga_active(14) = '0' and vga_select(14) = '1') then
					irgb <= rgb_sel; 
				elsif (vga_active(14) = '1' and vga_select(14) = '0') then
					irgb <= rgb_activ; 
				else
					irgb <= rgb_notactiv;
				end if;

			elsif (row(11 downto 6) = "000000100") and (col(11 downto 6) = "000000010") then
				if (vga_active(15) = '1' and vga_select(15) = '1') then
					irgb <= rgb_sel_act; 
				elsif (vga_active(15) = '0' and vga_select(15) = '1') then
					irgb <= rgb_sel; 
				elsif (vga_active(15) = '1' and vga_select(15) = '0') then
					irgb <= rgb_activ; 
				else
					irgb <= rgb_notactiv;
				end if;
			
			elsif (row(11 downto 6) = "000000100") and (col(11 downto 6) = "000000011") then
				if (vga_active(16) = '1' and vga_select(16) = '1') then
					irgb <= rgb_sel_act; 
				elsif (vga_active(16) = '0' and vga_select(16) = '1') then
					irgb <= rgb_sel; 
				elsif (vga_active(16) = '1' and vga_select(16) = '0') then
					irgb <= rgb_activ; 
				else
					irgb <= rgb_notactiv;
				end if;
			
			elsif (row(11 downto 6) = "000000100") and (col(11 downto 6) = "000000100") then
				if (vga_active(17) = '1' and vga_select(17) = '1') then
					irgb <= rgb_sel_act; 
				elsif (vga_active(17) = '0' and vga_select(17) = '1') then
					irgb <= rgb_sel; 
				elsif (vga_active(17) = '1' and vga_select(17) = '0') then
					irgb <= rgb_activ; 
				else
					irgb <= rgb_notactiv;
				end if;
			
			elsif (row(11 downto 6) = "000000100") and (col(11 downto 6) = "000000101") then
				if (vga_active(18) = '1' and vga_select(18) = '1') then
					irgb <= rgb_sel_act; 
				elsif (vga_active(18) = '0' and vga_select(18) = '1') then
					irgb <= rgb_sel; 
				elsif (vga_active(18) = '1' and vga_select(18) = '0') then
					irgb <= rgb_activ; 
				else
					irgb <= rgb_notactiv;
				end if;
			
			elsif (row(11 downto 6) = "000000100") and (col(11 downto 6) = "000000110") then -- 20
				if (vga_active(19) = '1' and vga_select(19) = '1') then
					irgb <= rgb_sel_act; 
				elsif (vga_active(19) = '0' and vga_select(19) = '1') then
					irgb <= rgb_sel; 
				elsif (vga_active(19) = '1' and vga_select(19) = '0') then
					irgb <= rgb_activ; 
				else
					irgb <= rgb_notactiv;
				end if;

			elsif (row(11 downto 6) = "000000101") and (col(11 downto 6) = "000000010") then
				if (vga_active(20) = '1' and vga_select(20) = '1') then
					irgb <= rgb_sel_act; 
				elsif (vga_active(20) = '0' and vga_select(20) = '1') then
					irgb <= rgb_sel; 
				elsif (vga_active(20) = '1' and vga_select(20) = '0') then
					irgb <= rgb_activ; 
				else
					irgb <= rgb_notactiv;
				end if;
			
			elsif (row(11 downto 6) = "000000101") and (col(11 downto 6) = "000000011") then
				if (vga_active(21) = '1' and vga_select(21) = '1') then
					irgb <= rgb_sel_act; 
				elsif (vga_active(21) = '0' and vga_select(21) = '1') then
					irgb <= rgb_sel; 
				elsif (vga_active(21) = '1' and vga_select(21) = '0') then
					irgb <= rgb_activ; 
				else
					irgb <= rgb_notactiv;
				end if;
			
			elsif (row(11 downto 6) = "000000101") and (col(11 downto 6) = "000000100") then
				if (vga_active(22) = '1' and vga_select(22) = '1') then
					irgb <= rgb_sel_act; 
				elsif (vga_active(22) = '0' and vga_select(22) = '1') then
					irgb <= rgb_sel; 
				elsif (vga_active(22) = '1' and vga_select(22) = '0') then
					irgb <= rgb_activ; 
				else
					irgb <= rgb_notactiv;
				end if;
			
			elsif (row(11 downto 6) = "000000101") and (col(11 downto 6) = "000000101") then
				if (vga_active(23) = '1' and vga_select(23) = '1') then
					irgb <= rgb_sel_act; 
				elsif (vga_active(23) = '0' and vga_select(23) = '1') then
					irgb <= rgb_sel; 
				elsif (vga_active(23) = '1' and vga_select(23) = '0') then
					irgb <= rgb_activ; 
				else
					irgb <= rgb_notactiv;
				end if;
			
			elsif (row(11 downto 6) = "000000101") and (col(11 downto 6) = "000000110") then -- 25
				if (vga_active(24) = '1' and vga_select(24) = '1') then
					irgb <= rgb_sel_act; 
				elsif (vga_active(24) = '0' and vga_select(24) = '1') then
					irgb <= rgb_sel; 
				elsif (vga_active(24) = '1' and vga_select(24) = '0') then
					irgb <= rgb_activ; 
				else
					irgb <= rgb_notactiv;
				end if; 
			
			else
				irgb <=  "000000000";

			end if;
	 

			if (row=128 or row=192 or row=254 or row=320 or col=192 or col=255 or col=320 or col=384 or 
				row=127 or row=191 or row=253 or row=319 or col=191 or col=254 or col=319 or col=383 or
				row=129 or row=193 or row=255 or row=321 or col=193 or col=256 or col=321 or col=385) then
				irgb <= "000000000";  
			end if;

		end if;
	
	end process;   

	--=========================================================================
end main;

