----------------------------------------------------------------------------------
-- Engineer: 
----------------------------------------------------------------------------------
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use work.mask_pack.ALL; -- vysledek z prvniho ukolu


entity cell is
   GENERIC (
      MASK              : mask_t := (others => '1') -- mask_t definovano v baliku math_pack
   );
   Port ( 
      INVERT_REQ_IN     : in   STD_LOGIC_VECTOR (3 downto 0);
      INVERT_REQ_OUT    : out  STD_LOGIC_VECTOR (3 downto 0);
      
      KEYS              : in   STD_LOGIC_VECTOR (4 downto 0);
      
      SELECT_REQ_IN     : in   STD_LOGIC_VECTOR (3 downto 0);
      SELECT_REQ_OUT    : out  STD_LOGIC_VECTOR (3 downto 0);
      
      INIT_ACTIVE       : in   STD_LOGIC;
      ACTIVE            : out  STD_LOGIC;
      
      INIT_SELECTED     : in   STD_LOGIC;
      SELECTED          : out  STD_LOGIC;

      CLK               : in   STD_LOGIC;
      RESET             : in   STD_LOGIC
   );
end cell;

architecture Behavioral of cell is
  constant IDX_TOP    : NATURAL := 0; -- index sousedni bunky nachazejici se nahore v signalech *_REQ_IN a *_REQ_OUT, index klavesy posun nahoru v KEYS
  constant IDX_LEFT   : NATURAL := 1; -- ... totez        ...                vlevo
  constant IDX_RIGHT  : NATURAL := 2; -- ... totez        ...                vpravo
  constant IDX_BOTTOM : NATURAL := 3; -- ... totez        ...                dole

  constant IDX_ENTER  : NATURAL := 4; -- index klavesy v KEYS, zpusobujici inverzi bunky (enter, klavesa 5)

  signal		SELECTED_buf : STD_LOGIC;
  signal		ACTIVE_buf : STD_LOGIC;
  --signal 	INVERT_REQ_OUT_buf: STD_LOGIC_VECTOR (3 downto 0);
begin
	SELECTED <= SELECTED_buf;
	ACTIVE <= ACTIVE_buf;
	--INVERT_REQ_OUT <= INVERT_REQ_OUT_buf;
	
	
	cell: process( clk )
	begin
-- Pozadavky na funkci (sekvencni chovani vazane na vzestupnou hranu CLK)
--   pri resetu se nastavi ACTIVE a SELECTED na vychozi hodnotu danou signaly INIT_ACTIVE a INIT_SELECTED
--   pokud je bunka aktivni a prijde signal z klavesnice, tak se bud presune aktivita pomoci SELECT_REQ na dalsi bunky nebo se invertuje stav bunky a jejiho okoli pomoci INVERT_REQ (klavesa ENTER)
--   pokud bunka neni aktivni a prijde signal INVERT_REQ, invertuje svuj stav
--   pozadavky do okolnich bunek se posilaji a z okolnich bunek prijimaji, jen pokud je maska na prislusne pozici v '1'
		


		if (RESET = '1') then
			ACTIVE_buf <= INIT_ACTIVE;
			--ACTIVE <= INIT_ACTIVE;
			--SELECTED <= INIT_SELECTED;
			SELECTED_BUF <= INIT_SELECTED;
		elsif (rising_edge(CLK)) then
			-- INIT REQUESTS
			SELECT_REQ_OUT <= "0000";
			INVERT_REQ_OUT <= "0000";
			--pokud je vybrana:
			if SELECTED_buf = '1' then

				--presun kurzou sousedovy
				if KEYS(IDX_TOP) = '1' and mask.top = '1' then
				--if SELECTED_buf = '1' then
					SELECT_REQ_OUT <= "0001";
					--SELECTED <= '0';
					SELECTED_buf <= '0';
				end if;
				if KEYS(IDX_LEFT) = '1' and mask.left = '1' then
					SELECT_REQ_OUT <= "0010";
					--SELECTED <= '0';
					SELECTED_buf <= '0';					
				end if;
				if KEYS(IDX_RIGHT) = '1' and mask.right = '1' then
					SELECT_REQ_OUT <= "0100";
					--SELECTED <= '0';
					SELECTED_buf <= '0';
				end if;
				if KEYS(IDX_BOTTOM) = '1' and mask.bottom = '1' then
					SELECT_REQ_OUT <= "1000";
					--SELECTED <= '0';
					SELECTED_buf <= '0';
				end if;
				if KEYS(IDX_ENTER) = '1' then
					--invertuj sebe a rekni ostatnim sousedum
					ACTIVE_buf <= not ACTIVE_buf;
					if mask.top = '1' then
						INVERT_REQ_OUT(0) <= '1';
					end if;
					if mask.left = '1' then
						INVERT_REQ_OUT(1) <= '1';
					end if;
					if mask.right = '1' then
						INVERT_REQ_OUT(2) <= '1';
					end if;
					if mask.bottom = '1' then
						INVERT_REQ_OUT(3) <= '1';
					end if;
				end if;
			end if;
			--bunka nevybrana, ale je pozadavek na inverzi stavu
			if SELECTED_buf = '0' and INVERT_REQ_IN = "0001" and mask.top = '1' then
				ACTIVE_buf <= not ACTIVE_buf;
				--ACTIVE_buf <= '1';
				--ACTIVE <= not ACTIVE;
			end if;
			if SELECTED_buf = '0' and INVERT_REQ_IN = "0010" and mask.left = '1' then
				ACTIVE_buf <= not ACTIVE_buf;
			end if;
			if SELECTED_buf = '0' and INVERT_REQ_IN = "0100" and mask.right = '1' then
				ACTIVE_buf <= not ACTIVE_buf;
			end if;
			if SELECTED_buf = '0' and INVERT_REQ_IN = "1000" and mask.bottom = '1' then
				ACTIVE_buf <= not ACTIVE_buf;
			end if;
			--nevybrana, ale je pozadavek na presun kurzoru na tuto bunku
			if SELECTED_buf = '0' and SELECT_REQ_IN = "0001" and mask.top = '1' then
				--SELECTED <= '1';
				SELECTED_buf <= '1';
			end if;
			if SELECTED_buf = '0' and SELECT_REQ_IN = "0010" and mask.left = '1' then
				SELECTED_buf <= '1';
			end if;
			if SELECTED_buf = '0' and SELECT_REQ_IN = "0100" and mask.right = '1' then
				SELECTED_buf <= '1';
			end if;
			if SELECTED_buf = '0' and SELECT_REQ_IN = "1000" and mask.bottom = '1' then
				SELECTED_buf <= '1';
			end if;
		end if;


		end process;
end Behavioral;
































